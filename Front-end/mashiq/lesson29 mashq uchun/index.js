




let user=[
    {
        name:'Bobur',
        lastName:'Bekmurodov',
        age:30,
        company:'Pdp',
        salary:20000,
    }
];
let edited=-1;
function person() {
    document.getElementById('tbody').innerHTML ='';
    for (let i=0; i<user.length; i++){
        document.getElementById('tbody').innerHTML +=
            '<tr>'+
            '<td>'+ (i+1) +'</td>'+
            '<td>'+ user[i].name +'</td>'+
            '<td>'+ user[i].lastName +'</td>'+
            '<td>'+ user[i].age +'</td>'+
            '<td>'+ user[i].company +'</td>'+
            '<td>'+ user[i].salary +'</td>'+
            '<td><button onclick="addEdit('+ i +')" class="btn btn-warning">E</button></td>'+
            '<td><button onclick="addDelete('+ i +')" class="btn btn-danger">D</button></td>'+
            '</tr>'
    }
}
person();

function result1() {
    let name=document.forms['myForm']['name'].value;
    let lastName=document.forms['myForm']['lastName'].value;
    let age=document.forms['myForm']['age'].value;
    let company=document.forms['myForm']['company'].value;
    let salary=document.forms['myForm']['salary'].value;
    let newUser={
        name,
        lastName,
        age,
        company,
        salary,
    };
    if (edited >= 0){
        user[edited]=newUser;

        edited=-1;
    }
    else {
        user.push(newUser);
    }
    if (name.trim().length === 0){
        alert('Ismingizni kiriting')
    }
    else if (lastName.trim().length === 0){
        alert('Familyangizni kiriting')
    }
    else if (age.trim().length === 0){
        alert('Yoshingizni kiriting')
    }
    else if (company.trim().length === 0){
        alert('Kompaniyangizni kiriting')
    }
    else if (salary.trim().length === 0){
        alert('Maoshingizni  kiriting')
    }
    else {

        person();
        document.forms['myForm'].reset();
    }

}

function addDelete(index) {
    user.splice(index,1);
    person();
}

function addEdit(index1) {
    document.forms['myForm']['name'].value=user[index1].name;
    document.forms['myForm']['lastName'].value=user[index1].lastName;
    document.forms['myForm']['age'].value=user[index1].age;
    document.forms['myForm']['company'].value=user[index1].company;
    document.forms['myForm']['salary'].value=user[index1].salary;
    edited=index1;
}
