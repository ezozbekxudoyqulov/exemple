


tasks = [
    {
    task: 'Mobile',
    startDate: '23.06.2021',
    endDate: '30.06.2021',
    employee: 'Nizom',
    status: 'pending',
    isRejected:false
}
];
function drawList() {
    document.getElementById('pendingTask').innerHTML ='';
    document.getElementById('doingTask').innerHTML ='';
    document.getElementById('doneTask').innerHTML ='';
    document.getElementById('rejectedTask').innerHTML ='';
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].status === 'pending') {
            document.getElementById('pendingTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task+ (tasks[i].isRejected ? '<span class="badge badge-danger badge-pill">rejected</span>':'')+'</h6>' +
                '<h6>'+'Xodim:' + tasks[i].employee+'</h6>' +
                '<h6>'+'startDate:' + tasks[i].startDate+'</h6>' +
                '<h6>'+'endDate:' + tasks[i].endDate+'</h6>' +
                '<select id="select'+i+'" class="form-control">' +
                '<option disabled selected>tanlang</option>' +
                '<option value="doing">doing</option>' +
                '<option value="done">done</option>' +
                '</select>'+
                '<button type="button" onclick="editTask('+ i +')"  class="btn btn-warning mt-3">Edit</button>'+
                '<button type="button" onclick="deleteTask('+ i +')" class="btn btn-danger ml-3 mt-3">Delete</button>'+
                '</div>'
        }
        else if (tasks[i].status === 'doing') {
            document.getElementById('doingTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task+(tasks[i].isRejected ? '<span class="badge bg-danger badge-pill text-white">rejected </span>':'')+'</h6>' +
                '<h6>'+'Xodim:' + tasks[i].employee+'</h6>' +
                '<h6>'+'startDate:' + tasks[i].startDate+'</h6>' +
                '<h6>'+'endDate:' + tasks[i].endDate+'</h6>' +
                '<select id="select'+i+'"   class="form-control">' +
                '<option disabled selected>tanlang</option>' +
                '<option value="pending">pending</option>' +
                '<option value="done">done</option>' +
                '</select>'+
                '<button type="button" onclick="editTask('+ i +')" class="btn btn-warning mt-3">Edit</button>'+
                '<button type="button" onclick="deleteTask('+ i +')" class="btn btn-danger ml-3 mt-3">Delete</button>'+

                '</div>'

        }
        else if (tasks[i].status === 'done') {
            document.getElementById('doneTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task+ (tasks[i].isRejected ? '<span class="badge bg-danger badge-pill text-white">rejected</span>':"")+'</h6>' +
                '<h6>'+'Xodim:' + tasks[i].employee+'</h6>' +
                '<h6>'+'startDate:' + tasks[i].startDate+'</h6>' +
                '<h6>'+'endDate:' + tasks[i].endDate+'</h6>' +
                '<button onclick="rejected1('+i+')" type="button" class="btn btn-danger mt-3">Rejected</button>'+
                '</div>'
        }
        else {
            document.getElementById('rejectedTask').innerHTML +=
                '<div>' +
                '<h6>' + tasks[i].task+'</h6>' +
                '<h6>'+'Xodim:' + tasks[i].employee+'</h6>' +
                '<h6>'+'startDate:' + tasks[i].startDate+'</h6>' +
                '<h6>'+'endDate:' + tasks[i].endDate+'</h6>' +
                '<select id="select'+i+'"  class="form-control">' +
                '<option disabled selected>tanlang</option>' +
                '<option value="doing">doing</option>' +
                '<option value="pending">pending</option>' +
                '</select>'+
                '<button onclick="editTask('+ i +')"  type="button" class="btn btn-warning mt-3">Edit</button>'+
                '</div>'
        }
    }
}

drawList();

function answer() {
    let task=document.forms['myForm']['task'].value;
    let startDate=document.forms['myForm']['startDate'].value;
    let endDate=document.forms['myForm']['endDate'].value;
    let employee=document.forms['myForm']['employee'].value;
    let status=document.forms['myForm']['status'].value;
    if (task.trim().length>0 && startDate.trim().length>0 && endDate.trim().length>0 && employee!=='Tanlang'&&  status!=='Tanlang'){
        let newTask={
            task,
            startDate,
            endDate,
            employee,
            status,

        };
        tasks.push(newTask);
        drawList();
        document.forms['myForm'].reset();
    }else {
        alert('Formani to\'ldiring');
    }

}
function deleteTask(index) {
    tasks.splice(index,1);
    drawList();
}

function editTask(editingIndex) {
    if (document.getElementById('select'+editingIndex).value !=='tanlang'){
        tasks[editingIndex].status=document.getElementById('select'+editingIndex).value;
    }
    drawList()
}
function rejected1(index) {
    tasks[index].status='rejected';
    tasks[index].isRejected=true;
    drawList()
}



