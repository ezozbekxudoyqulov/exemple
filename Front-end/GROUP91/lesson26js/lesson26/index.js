let num1 = 5;
let num2 = 10;
let num3;
num3 = num1;  //5
num1 = num2; //10
num2 = num3; //5

console.log(num1);
console.log(num2);


/*Reference type*/
//object
let user = {
    firstName: 'Ali',
    lastName: 'Valiyev',
    age: 25,
    isMarried: false,
    group: 91,
    fullName: function () {
        return this.lastName + ' ' + this.firstName
    }
};
user.group = 90;
console.log(user);
console.log(user.age);
console.log(user['lastName']);

//arrays
let numbers = [5, 8, 45, 32, 87];
let colors = ['red', 'blue', 'white', 'green'];
let students = [
    {
        firstName: 'Nodir',
        age: 32,
        group: 85
    },
    {
        firstName: 'Qodir',
        age: 56,
        group: 99
    },
    {
        firstName: 'Botir',
        age: 28,
        group: 17
    },
];
numbers[2] = 55;
colors[3] = 'yellow';
students[2].firstName = students[2].firstName + 'jon';

console.log(numbers);
console.log(numbers.length);
console.log(numbers[2]);
console.log(colors[3]);
console.log(students);
console.log(students[2].firstName);

//function

function plus(x, y, z) {
    console.log(x + y + z);
}

plus(20, 10, 25);

const pi = 3.14;
console.log(pi);


document.getElementById('group').innerHTML = user.group;
document.getElementById('age').innerHTML = user.age;
document.getElementById('userName').innerHTML = user.fullName();


let num10 = 58;
console.log(5 === '5');
console.log(10 >= 4);

// mantiqiy yoki ||  +
//mantiqiy va   &&   *
console.log(false || false);  // 1 || 0
console.log(true && true);  // 1 || 0

let hour = 7;
let result = hour > 9 ? 'Dars boshlandi' : 'Dars boshlanmadi';
console.log(result);


let ball = 86;
if (ball >= 56 && ball < 71) {
    console.log('Balingiz: 3')
} else if (ball >= 71 && ball < 86) {
    console.log('Balingiz: 4')
} else if (ball >= 86 && ball <= 100) {
    console.log('Balingiz: 5')
} else {
    console.log('Yiqildingiz')
}

let word = 'ruler';

switch (word) {
    case 'pencil':
        console.log('qalam');
        break;
    case "pen":
        console.log('ruchka');
        break;
    case "ruler":
        console.log('chizgich');
        break;
    default:
        console.log('Bu sozni tarjimasi yoq')
}



let year=2024;

if (year % 4 === 0 && year%100 !==0 || year%400===0){
    console.log(366)
}
else {
    console.log(365)
}

//
// if (number%2===0){
//     console.log('juftson');
// }
// else  if (number%2 !==0 ){
//     console.log('toqson');
// }
// else{
//     console.log( 0+'teng')
// }



let number=111;

if (number>10 && number<=99){
  if (number%2===0){
      console.log(2+ 'j')
  }
  else {
      console.log(2+ 't')
  }
}
 else  if (number>99 && number<1000){
   if (number%2===0){
       console.log(3+'j')
   }
   else {
       console.log(3+'t')
   }
}
 else {
    console.log('berilgan son uch xonali emas')
}








 let book=7;
 let isWeekend=true;
 if ((book>=40 && book<=60) || book>60 && isWeekend===true ){
     console.log(true)
 }

else{
    console.log(false)
 }
console.log('tttt');
let cigars=66666;

if((cigars>=40 && cigars<=60) || cigars>60 &&  isWeekend===true ){
    console.log(true)
}
else{
    console.log(false)


}


function hello() {
   let natija=document.getElementById('myInput').value;
   document.getElementById('myValue').innerHTML=natija;
}





function weekDay() {
    let week= +document.getElementById('input').value;
    switch (week) {
        case 1:
            document.getElementById('day').innerHTML='Dushanba';
            break;
        case 2:
            document.getElementById('day').innerHTML='seshanba';
            break;
        case 3:
            document.getElementById('day').innerHTML='Chorshanba';
            break;
        case 4:
            document.getElementById('day').innerHTML='Payshanba';
            break;
        case 5:
            document.getElementById('day').innerHTML='Juma';
        break;

        case 6:
            document.getElementById('day').innerHTML='Shanba';
            break;
        case 7:
            document.getElementById('day').innerHTML='Yakshanba';
            break;
        default:
            document.getElementById('day').innerHTML="bunday kun yo'q";

    }
}














