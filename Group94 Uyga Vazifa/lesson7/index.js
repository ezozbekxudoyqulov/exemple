let mevalar = [];

function draw() {
    document.getElementById('tbody').innerHTML = '';
    for (let i = 0; i < mevalar.length; i++) {
        document.getElementById('tbody').innerHTML +=
            '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + mevalar[i].nomi + '</td>' +
            '<td>' + mevalar[i].kg + '</td>' +
            '<td>' + mevalar[i].narxi + '</td>' +
            '<td><button onclick="deleted(' + i + ')" type="button" class="btn btn-danger">Delete</button></td>' +
            '<td><button onclick="Edited(' + i + ')" type="button" class="btn btn-warning">Edit</button></td>' +
            '</tr>'

    }
}

draw();

function meva() {
    let nomi = document.forms['myForm']['nomi'].value;
    let kg = document.forms['myForm']['kg'].value;
    let narxi = document.forms['myForm']['narxi'].value;
    if (nomi.trim().length > 0 && kg.trim().length > 0 && narxi.trim().length > 0)
    {
        let newMeva = {
            nomi,
            kg,
            narxi,
        };
        if (edited>=0){
            mevalar[edited]=newMeva;
            edited=-1;
        }
        else {
            mevalar.push(newMeva);
        }
        draw();
        document.forms['myForm'].reset();
    } else {
        alert('Formani to\'liq to\'ldiring');
    }

}

function deleted(index) {
    mevalar.splice(index, 1);
    draw();
}
let edited=-1;
function Edited(index) {
    document.forms['myForm']['nomi'].value=mevalar[index].nomi;
    document.forms['myForm']['kg'].value=mevalar[index].kg;
    document.forms['myForm']['narxi'].value=mevalar[index].narxi;
    edited=index
}
