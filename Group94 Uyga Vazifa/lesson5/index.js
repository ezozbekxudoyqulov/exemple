function week() {
    let number = +document.getElementById('int3').value;
    switch (number) {
        case 1:
            document.getElementById('res1').innerHTML = ('Dushanba');
            break;
        case 2:
            document.getElementById('res1').innerHTML = ('Seshanba');
            break;
        case 3:
            document.getElementById('res1').innerHTML = ('Chorshanba');
            break;
        case 4:
            document.getElementById('res1').innerHTML = ('Payshanba');
            break;
        case 5:
            document.getElementById('res1').innerHTML = ('Juma');
            break;
        case 6:
            document.getElementById('res1').innerHTML = ('Shanba');
            break;
        case 7:
            document.getElementById('res1').innerHTML = ('Yakshanba');
            break;
        default:
            document.getElementById('res1').innerHTML = ('Bunday hafta kun yo\'q');
    }
}


function ball() {
    let l = +document.getElementById('int4').value;
    switch (l) {
        case 1:
            document.getElementById('res2').innerHTML = ('Yomon');
            break;
        case 2:
            document.getElementById('res2').innerHTML = ('Qoniqarsiz');
            break;
        case 3:
            document.getElementById('res2').innerHTML = ('Qomiqarli');
            break;
        case 4:
            document.getElementById('res2').innerHTML = ('Yaxshi');
            break;
        case 5:
            document.getElementById('res2').innerHTML = ('A\'lo');
            break;
        default:
            document.getElementById('res2').innerHTML = ('Xato')

    }
}

function month1() {
    let month = +document.getElementById('int5').value;
    switch (month) {
        case 1:
        case 2:
        case 12:
            document.getElementById('res3').innerHTML = ('Qish');
            break;
        case 3:
        case 4:
        case 5:
            document.getElementById('res3').innerHTML = ('Bahor');
            break;
        case 6:
        case 7:
        case 8:
            document.getElementById('res3').innerHTML = ('Yoz');
            break;
        case 9:
        case 10:
        case 11:
            document.getElementById('res3').innerHTML = ('Kuz');
            break;
        default:
            document.getElementById('res3').innerHTML = ('Bunday oy yo\'q')

    }
}

function monthDay() {
    let day = +document.getElementById('int6').value;
    switch (day) {
        case 1:
            document.getElementById('res4').innerHTML = ('Yanvar ' + 31 + ' kun');
            break;
        case 2:
            document.getElementById('res4').innerHTML = ('Fevral ' + 28 + ' kun');
            break;
        case 12:
            document.getElementById('res4').innerHTML = ('Dekabr ' + 31 + ' kun');
            break;
        case 3:
            document.getElementById('res4').innerHTML = ('Mart ' + 31 + ' kun');
            break;
        case 4:
            document.getElementById('res4').innerHTML = ('Aprel ' + 30 + ' kun');
            break;
        case 5:
            document.getElementById('res4').innerHTML = ('May ' + 31 + ' kun');
            break;
        case 6:
            document.getElementById('res4').innerHTML = ('Iyun ' + 30 + ' kun');
            break;
        case 7:
            document.getElementById('res4').innerHTML = ('Iyul ' + 31 + ' kun');
            break;
        case 8:
            document.getElementById('res4').innerHTML = ('Avgust ' + 31 + ' kun');
            break;
        case 9:
            document.getElementById('res4').innerHTML = ('Sentabr ' + 30 + ' kun');
            break;
        case 10:
            document.getElementById('res4').innerHTML = ('Oktabr ' + 31 + ' kun');
            break;
        case 11:
            document.getElementById('res4').innerHTML = ('Noyabr ' + 30 + ' kun');
            break;
        default:
            document.getElementById('res4').innerHTML = ('Bunday oy yo\'q')

    }
}

function oper() {
    let a = +document.getElementById('int7').value;
    let b = +document.getElementById('int8').value;
    let res = +document.getElementById('int9').value;
    switch (res) {
        case 1:
            document.getElementById('res5').innerHTML = a + b;
            break;
        case 2:
            document.getElementById('res5').innerHTML = a - b;
            break;
        case 3:
            document.getElementById('res5').innerHTML = a / b;
            break;
        case 4:
            document.getElementById('res5').innerHTML = a * b;
            break;
    }
}

function hisoblash() {
    let a = +document.getElementById('int1').value;
    let b = +document.getElementById('int2').value;
    if (a > 0 && a <= 5 && b > 0) {
        if (a === 1) {
            document.getElementById('res').innerHTML = b / 10 + ' m';
        } else if (a === 2) {
            document.getElementById('res').innerHTML = b * 1000 + ' m';
        } else if (a === 3) {
            document.getElementById('res').innerHTML = b + ' m';
        } else if (a === 4) {
            document.getElementById('res').innerHTML = b / 1000 + ' m';
        } else if (a === 5) {
            document.getElementById('res').innerHTML = b / 100 + ' m';
        }
    } else {
        alert('Formani to\'ldiring')
    }
}

function hisoblash1() {
    let a = +document.getElementById('inp1').value;
    let b = +document.getElementById('inp2').value;
    if (a > 0 && a <= 5 && b > 0) {
        if (a === 1) {
            document.getElementById('re').innerHTML = b + ' kg';
        } else if (a === 2) {
            document.getElementById('re').innerHTML = b / 10000 + ' kg';
        } else if (a === 3) {
            document.getElementById('re').innerHTML = b / 1000 + ' kg';
        } else if (a === 4) {
            document.getElementById('re').innerHTML = b * 1000 + ' kg';
        } else if (a === 5) {
            document.getElementById('re').innerHTML = b * 100 + ' kg';
        }
    } else {
        alert('Formani to\'ldiring')
    }
}

let inp = '';
let inp2 = '';

function day1() {
    inp = +document.getElementById('inp5').value;
    inp2 = +document.getElementById('inp6').value;
    switch (inp) {
        case 1:
            document.getElementById('re4').innerHTML = 1;
            break;
        case 2:
            document.getElementById('re4').innerHTML = 2;
            break;
        case 3:
            document.getElementById('re4').innerHTML = 3;
            break;
        case 4:
            document.getElementById('re4').innerHTML = 4;
            break;
        case 5:
            document.getElementById('re4').innerHTML = 5;
            break;
        case 6:
            document.getElementById('re4').innerHTML = 6;
            break;
        case 7:
            document.getElementById('re4').innerHTML = 7;
            break;
        case 8:
            document.getElementById('re4').innerHTML = 8;
            break;
        case 9:
            document.getElementById('re4').innerHTML = 9;
            break;
        case 10:
            document.getElementById('re4').innerHTML = 10;
            break;
        case 11:
            document.getElementById('re4').innerHTML = 11;
            break;
        case 12:
            document.getElementById('re4').innerHTML = 12;
            break;
        case 13:
            document.getElementById('re4').innerHTML = 13;
            break;
        case 14:
            document.getElementById('re4').innerHTML = 14;
            break;
        case 15:
            document.getElementById('re4').innerHTML = 15;
            break;
        case 16:
            document.getElementById('re4').innerHTML = 16;
            break;
        case 17:
            document.getElementById('re4').innerHTML = 17;
            break;
        case 18:
            document.getElementById('re4').innerHTML = 18;
            break;
        case 19:
            document.getElementById('re4').innerHTML = 19;
            break;
        case 20:
            document.getElementById('re4').innerHTML = 20;
            break;
        case 21:
            document.getElementById('re4').innerHTML = 21;
            break;
        case 22:
            document.getElementById('re4').innerHTML = 22;
            break;
        case 23:
            document.getElementById('re4').innerHTML = 23;
            break;
        case 24:
            document.getElementById('re4').innerHTML = 24;
            break;
        case 25:
            document.getElementById('re4').innerHTML = 25;
            break;
        case 26:
            document.getElementById('re4').innerHTML = 26;
            break;
        case 27:
            document.getElementById('re4').innerHTML = 27;
            break;
        case 28:
            document.getElementById('re4').innerHTML = 28;
            break;
        case 29:
            document.getElementById('re4').innerHTML = 29;
            break;
        case 30:
            document.getElementById('re4').innerHTML = 30;
            break;
        case 31:
            document.getElementById('re4').innerHTML = 31;

    }
    switch (inp2) {
        case 1:
            document.getElementById('re4').innerHTML += ' Yanvar';
            break;
        case 2:
            document.getElementById('re4').innerHTML += ' Fevral';
            break;
        case 3:
            document.getElementById('re4').innerHTML += ' Mart';
            break;
        case 4:
            document.getElementById('re4').innerHTML += ' Aprel';
            break;
        case 5:
            document.getElementById('re4').innerHTML += ' May';
            break;
        case 6:
            document.getElementById('re4').innerHTML += ' Iyun';
            break;
        case 7:
            document.getElementById('re4').innerHTML += ' Iyul';
            break;
        case 8:
            document.getElementById('re4').innerHTML += ' Avgust';
            break;
        case 9:
            document.getElementById('re4').innerHTML += ' Sentabr';
            break;
        case 10:
            document.getElementById('re4').innerHTML += ' Oktabr';
            break;
        case 11:
            document.getElementById('re4').innerHTML += ' Noyabr';
            break;
        case 12:
            document.getElementById('re4').innerHTML += ' Dekabr';
            break;
    }
}

let a = 'Hello World world How are you? ';
let b = 'Hello World World';
console.log(a.search(/world/));
let d = (a.replace(/world|hello|how/ig, function (soz) {
    if (soz === "hello" || soz === 'Hello') {
        return 'book';
    } else if (soz === 'world' || soz === 'World') {
        return 'reading';
    } else if (soz === 'how' || soz === 'How') {
        return 'ok';
    }
}));
console.log(d);




