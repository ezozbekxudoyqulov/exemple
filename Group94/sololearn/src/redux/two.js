import React from 'react';
import {connect} from "react-redux";

function Two(props) {
    return (
        <div><h1>{props.inputValue}</h1></div>
    );
}
function mapStateToProps(state) {
   return{
       inputValue:state.inputValue
   }
}

export default connect(mapStateToProps,null) (Two);