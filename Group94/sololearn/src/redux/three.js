import React from 'react';
import {connect} from "react-redux";

function Three(props) {
    console.log(props.inputValue)
    function getInputValue(e) {
        props.setInputVal(e.target.value)
    }
    return (
        <div><input onChange={getInputValue} style={{margin:"100px"}} type="text"/>
        <h1>{props.inputValue}</h1></div>
    );
}
function mapStateToProps(state) {
    return{
        inputValue:state.inputValue
    }
}
function mapDispatchToProps(dispatch) {
    return{
        setInputVal:(e)=>{
          dispatch({
              type:"INPUT_VALUE",
              payload:{
                  inputValue:e
              }
          })

        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps) (Three);