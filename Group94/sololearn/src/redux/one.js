import React from 'react';
import {connect} from "react-redux";

function One(props) {
    console.log(props.count + "count value")
    return (
        <div><h1>{props.count}</h1>
 <button onClick={props.addCounter}>asasa</button>
 <button onClick={props.removeCounter}>kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk</button>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        count: state.count
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addCounter:()=>{
            dispatch({
                type:"add",
                payload:{
                    count:100
                }
            })
        },
        removeCounter:()=>{
            dispatch({
                type:"remove",
                payload:{
                    count: 100
                }
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(One);