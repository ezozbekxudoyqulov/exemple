import React from 'react';
import One from "./one";
import Two from "./two";
import {createStore} from "redux";
import {Provider} from "react-redux"
import Three from "./three";

function Redux(props) {
    function countReducer(state = {count: 0,inputValue:''}, action) {
        switch (action.type) {
            case "add":
                state = {
                    ...state,
                    count: state.count + action.payload.count
                }
                break;
            case "remove":
                state={
                    ...state,
                    count: state.count-action.payload.count
                }
                break;
            case "INPUT_VALUE":
                state={
                    ...state,
                 inputValue:action.payload.inputValue
                }
                break;
            default:

        }
        return state;
    }


    const store = createStore(countReducer);
    return (
        <Provider store={store}>
            <One/>
            <Two/>
            <Three/>
        </Provider>
    );
}

export default Redux;


