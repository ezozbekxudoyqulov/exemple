import React from 'react';
import {Layout, Menu} from 'antd';
import "antd/dist/antd.css";
import "./main.css";
import {LaptopOutlined, NotificationOutlined, UserOutlined,} from '@ant-design/icons';
import {Link, Route, Switch} from "react-router-dom";
import AddCourse from "../course/add-course";
import ListCourse from "../course/list-course";
import Lesson from "../lesson/lesson";


const { Sider, Content} = Layout;
const {SubMenu} = Menu;

function Main(props) {

    return (
        <>
            <Layout>
                <Sider>
                    <div className="logo"/>
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={['1']}
                        defaultOpenKeys={['sub1']}
                        style={{height: '100%', borderRight: 0}}
                    >
                        <SubMenu key="sub1" icon={<UserOutlined/>} title="course">
                            <Menu.Item key="1">
                                <Link to={'/admin/main/course/add'}>add</Link>
                            </Menu.Item>
                            <Menu.Item key="2">
                                <Link to={'/admin/main/course/list'}>list</Link>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub2" icon={<LaptopOutlined/>} title="lesson">
                            <Menu.Item key="3">
                                <Link to={'/admin/main/lesson/add'}>add</Link>
                            </Menu.Item>
                            <Menu.Item key="4">
                                <Link to={'/admin/main/lesson/list'}>list</Link>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub3" icon={<NotificationOutlined/>} title="subnav 3">
                            <Menu.Item key="9">option9</Menu.Item>
                            <Menu.Item key="10">option10</Menu.Item>
                            <Menu.Item key="11">option11</Menu.Item>
                            <Menu.Item key="12">option12</Menu.Item>
                        </SubMenu>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                        <Switch>
                            <Route path={'/admin/main/lesson/add'} component={Lesson}/>
                            <Route path={'/admin/main/course/add'} component={AddCourse}/>
                            <Route path={'/admin/main/course/list'} component={ListCourse}/>
                        </Switch>

                    </Content>
                </Layout>
            </Layout>
        </>
    );
}

export default Main;