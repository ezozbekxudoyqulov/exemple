import React, {useState} from 'react';
import "./authentication.scss";
import {Link, useHistory} from "react-router-dom";
import axios from "axios";

function Authentication(props) {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [text, setText] = useState('');

    const history  = useHistory();

    function login(){
        axios.post("/api/sololearn/user/login", {username: username,password: password}).then((response)=>{
            setText(response.data.message);

            if (response.data.status_code === 0){
                localStorage.setItem("token",response.data.data.token);
                if(response.data.data.is_user)
                    history.push("/");
                else  history.push("/admin/main")
            }
        }).catch((error)=>{
            if(error.response.status >= 500)
                setText("vaqtincha server bilan ulanishda xatolik")
        })
    }


    return (
        <>
            <div className="login">
            <div className="container">
                <div className="row">
                    <h1>welcome to solo learn admin</h1>
                    <div className="col-md-6 offset-3 mt-4">
                        <div className="card form">
                            <div className="card-body">
                                <h3 className={'text-danger text-center'}>{text}</h3>
                                <div className="form-group mt-2">
                                    <label htmlFor="username" className="text-black">username</label>
                                    <input onChange={(e)=>setUsername(e.target.value)} type="text" id={'username'} className={'form-control'}/>
                                </div>
                                <div className="form-group mt-2">
                                    <label htmlFor="password" className="text-black">password</label>
                                    <input onChange={(e)=>setPassword(e.target.value)} type="password" id={'password'} className={'form-control'}/>
                                </div>
                                <div className={'d-flex mt-2 justify-content-between align-items-center'}>
                                    <button className="btn mt-2 btn-primary mt-4 fs-4" onClick={login}>login</button>
                                    <Link to={'/admin/register'}>register</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </>
    );
}

export default Authentication;