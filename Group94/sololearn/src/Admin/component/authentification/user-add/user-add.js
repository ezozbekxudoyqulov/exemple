import React, {useReducer} from 'react';
import {useHistory} from "react-router-dom";
import axios from "axios";
import "./user.scss"

const ACTIONS = {
    USER_ADD: "user-add"
};

function reducer(state, action) {
    switch (action.type) {
        case ACTIONS.USER_ADD:
            state = {
                user: {
                    ...state.user,
                    [action.payload.key]: action.payload.value
                }
            };
            break;
        default:
    }
    return state;
}

function UserAdd(props) {

    const history = useHistory();

    const initialUser = {
        username: "",
        phone_number: "",
        password: "",
        full_name: "",
        user_role: ""
    };

    const [state, dispatch] = useReducer(reducer, {user: initialUser});

    function getInputValues(e){
        dispatch({
            type: ACTIONS.USER_ADD,
            payload: {
                key: e.target.name,
                value: e.target.value
            }
        })
    }

    function addUser(){
        axios.post("/api/sololearn/user/add",state.user).then((response)=>{
            if (response.data.status_code === 0)
                history.push("/admin/login")
        })
    }

    return (

        <div className="user">
        <div className="container">
            <div className="row">
                <h1 className={'text-center text-primary  text-uppercase'}>welcome to solo learn admin</h1>
                <div className="col-md-6 offset-3">
                    <div className="card form">
                        <div className="card-body">
                            <div className="form-group mt-2">
                                <label htmlFor="full_name" >full_name</label>
                                <input name={'full_name'} onChange={getInputValues} type="text"
                                       id={'username'} className={'form-control'}/>
                            </div>
                            <div className="form-group mt-2">
                                <label htmlFor="username" >username</label>
                                <input name={'username'} onChange={getInputValues} type="text"
                                       id={'username'} className={'form-control'}/>
                            </div>
                            <div className="form-group mt-2">
                                <label htmlFor="password">password</label>
                                <input name={'password'} onChange={getInputValues} type="password"
                                       id={'password'} className={'form-control'}/>
                            </div>
                            <div className="form-group mt-2">
                                <label htmlFor="password">phone_number</label>
                                <input name={'phone_number'} onChange={getInputValues}
                                       type="tel" id={'password'} className={'form-control'}/>
                            </div>
                            <div className="form-group mt-2">
                                <label htmlFor="password">user_role</label>
                                <select name="user_role" className={'form-control'} id="" onClick={getInputValues}>
                                    <option value="ADMIN">ADMIN</option>
                                    <option value="SUPER_ADMIN">SUPER_ADMIN</option>
                                    <option value="USER">USER</option>
                                </select>
                            </div>
                            <button onClick={addUser} className={'btn btn-primary'}>add user</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default UserAdd;