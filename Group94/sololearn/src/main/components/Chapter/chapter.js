import React, {useEffect, useState} from 'react';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import "./chapter.scss"
import axios from "axios";
import {Link} from "react-router-dom";
import Navbar from "../Home/Navbars/Navbar";

// import Accordion from 'react-responsive-accordion';
function Chapter(props) {

    const [chapter, setChapter] = useState([]);


    useEffect(() => {

        axios.get("/api/sololearn/course/chapter/" + props.match.params.id).then((res) => {
            setChapter(res.data.data)
        })
    }, []);

    return (
        <div className="chapter">
            <nav className="navbar desktop2" style={{background: "#F9F9FA"}}>
                <div className="container-fluid Wrapper">
                    <Link exact={true} to={"/"} className="navbar-brand" href="#">
                        <img src="img/logo-white.png" alt="logo"/>
                    </Link>
                    <ul className="nav">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to={"/course"}>Courses</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link">Code Playground</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Discuss</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Blog</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Get Pro</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/admin/login"}>
                                <button className="login">Log in</button>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/admin/register"}>
                                <button className="register">Register</button>
                            </Link>

                        </li>
                    </ul>
                </div>
            </nav>
            <div className="container">
                <Accordion>
                    {chapter.map((item, index) => {
                        return <AccordionItem key={index}>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                     <div className="accordion-button-name">
                                         <div className="img"><img src={item.image_url} alt="error"/></div>
                                         <div className="name">{item.name}</div>
                                     </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                                <p>
                                    Exercitation in fugiat est ut ad ea cupidatat ut in
                                    cupidatat occaecat ut occaecat consequat est minim minim
                                    esse tempor laborum consequat esse adipisicing eu
                                    reprehenderit enim.
                                </p>
                            </AccordionItemPanel>
                        </AccordionItem>
                    })}
                </Accordion>
            </div>
        </div>
    );
}

export default Chapter;