import React, {useEffect, useState} from 'react';
import axios from "axios";
import {Link} from "react-router-dom";
import "./course.scss"
function Course(props) {

    const [courseList, setCourseList] = useState([]);

    useEffect(() => {
        axios.get("/api/sololearn/course/list").then((response) => {
            setCourseList(response.data.data)
        })
    }, []);




    return (
        <div>
            <nav className="navbar desktop2" style={{background: "#F9F9FA"}}>
                <div className="container-fluid Wrapper">
                    <Link exact={true} to={"/"} className="navbar-brand" href="#">
                        <img src="img/logo-white.png" alt="logo-black"/>
                    </Link>
                    <ul className="nav">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to={"/course"}>Courses</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link">Code Playground</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Discuss</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Blog</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link"> Get Pro</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/admin/login"}>
                                <button className="login">Log in</button>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/admin/register"}>
                                <button className="register">Register</button>
                            </Link>

                        </li>
                    </ul>
                </div>
            </nav>
            <h1 className="text-center fw-bold my-5 py-5">What would you like to learn?</h1>
            <div className="container mt-5">
                <div className="row">
                    {courseList.map((item, index) => {

                        return <div className='col-md-6 col-lg-3' >
                                <Link to={"/course/chapter"+item.id}>
                                <div className="course">
                                    <div className="radius">
                                        <img src={item.image_url} alt="img"/>
                                    </div>
                                    <p className="text">{item.name}</p>
                                </div>
                                </Link>
                            </div>

                    })}
                </div>
            </div>
        </div>
    );
}

export default Course;