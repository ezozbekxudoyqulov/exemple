import React from 'react';
import './header.scss'
import {Link} from "react-router-dom";
import Zoom from 'react-reveal/Zoom';

function Header(props) {
    return (
        <>
            <header>
                <div className="container-fluid">
                    <Zoom>
                    <div className="row1 d-flex">
                        <div className=" left ">
                            <img src="img/star-left.png" alt=""/>
                        </div>
                        <div className=" centre ">
                            <h1 className="title">
                                The best way to learn to code
                            </h1>
                            <p className="text">
                                Courses designed by experts with real-world
                                practice. Join our global community. <span>It's free.</span>
                            </p>
                            <Link to={"/course"}>
                                <button className="header-btn">Start learning now!</button>
                            </Link>
                        </div>
                        <div className=" right">
                            <img src="img/star-right.png" alt=""/>
                        </div>
                    </div>
                    </Zoom>
                </div>
            </header>
        </>
    );
}

export default Header;