import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import "./Navbar.scss"

function Navbar(props) {
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click)
    let [navbar, setNavbar] = useState(window.scrollY);
    setInterval(() => {
        setNavbar(window.scrollY);
    }, 1);
    return (<>

            {navbar > 600 ? <nav className="navbar desktop2" style={{background:"#F9F9FA"}}>
                <div className="container-fluid Wrapper">
                <Link exact={true} to={"/"} className="navbar-brand" href="#">
                <img src="img/logo-white.png" alt="logo-black"/>
                </Link>
                <ul className="nav" >
                <li className="nav-item">
                <Link  className="nav-link active" aria-current="page" to={"/course"} >Courses</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link" >Code Playground</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link"> Discuss</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link"> Blog</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link"> Get Pro</Link>
                </li>
                <li className="nav-item">
                <Link to={"/admin/login"}>
                <button className="login">Log in</button>
                </Link>
                </li>
                <li className="nav-item">
                <Link to={"/admin/register"}>
                <button className="register">Register</button>
                </Link>

                </li>
                </ul>
                </div>
                </nav> : <nav className="navbar desktop" style={{background:"#1f1e28"}}>
                    <div className="container-fluid Wrapper">
                        <Link exact={true} to={"/"} className="navbar-brand" href="#">
                            <img src="img/logo-black.png" alt="logo-black"/>
                        </Link>
                        <ul className="nav">
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={"/course"}>Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" href="#">Code Playground</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Discuss</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Blog</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Get Pro</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/admin/login"}>
                                    <button className="login">Log in</button>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/admin/register"}>
                                    <button className="register">Register</button>
                                </Link>

                            </li>
                        </ul>
                    </div>
                </nav>}
            {navbar > 600 ? <nav className="navbar tablet" style={{background:"#F9F9FA"}}>
                <div className="container-fluid mobile">
                    <Link exact={true} to={"/"} className="navbar-brand tablet-brand" href="#">
                        <img src="img/logo-white.png" alt="logo-black"/>
                    </Link>
                    <div className="left">
                        <span style={{color:"#2d3846"}}>Menu</span>
                        <div className="menu-icon" onClick={handleClick}>
                            <i  style={{color:"#2d3846"}} className={click ? "fas fa-times" : "fas fa-bars"}/>
                        </div>
                    </div>
                </div>
                {click ? <div className="card" style={{border:"none",boxShadow:"1px 5px 10px grey"}}>
                    <div className="card-body" style={{background:"#ffffff"}}>
                        <ul className="nav">
                            <li className="nav-item">
                                <Link className="nav-link active" style={{color:"#47505C"}}  aria-current="page" to={"/course"}>Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}} href="#">Code Playground</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Discuss</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Blog</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Get Pro</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="card-footer" style={{background:"#ffffff"}}>

                        <Link to={"/admin/login"}>
                            <button className="login">Log in</button>
                        </Link>
                        <Link to={"/admin/register"}>
                            <button className="register">Register</button>
                        </Link>
                    </div>
                </div> : ''}
            </nav> : <nav className="navbar tablet">
                <div className="container-fluid mobile">
                    <Link exact={true} to={"/"} className="navbar-brand tablet-brand" href="#">
                        <img src="img/logo-black.png" alt="logo-black"/>
                    </Link>
                    <div className="left">
                        <span>Menu</span>
                        <div className="menu-icon" onClick={handleClick}>
                            <i className={click ? "fas fa-times" : "fas fa-bars"}/>
                        </div>
                    </div>
                </div>
                {click ? <div className="card">
                    <div className="card-body">
                        <ul className="nav">
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={"/course"}>Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link">Code Playground</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Discuss</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Blog</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Get Pro</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="card-footer">

                        <Link to={"/admin/login"}>
                            <button className="login">Log in</button>
                        </Link>
                        <Link to={"/admin/register"}>
                            <button className="register">Register</button>
                        </Link>
                    </div>
                </div> : ''}
            </nav>}
            {navbar > 600 ? <nav className="navbar mobile-navbar" style={{background:"#F9F9FA"}}>
                <div className="container-fluid mobile">
                    <Link exact={true} to={"/"} className="navbar-brand" href="#">
                        <img src="img/mobile-logo-w.png" alt="logo-black"/>
                    </Link>
                    <div className="left">
                        <span style={{color:"#2d3846"}}>Menu</span>
                        <div className="menu-icon" onClick={handleClick}>
                            <i style={{color:"#2d3846"}} className={click ? "fas fa-times" : "fas fa-bars"}/>
                        </div>
                    </div>
                </div>
                {click ?  <div className="card" style={{border:"none",boxShadow:"0px 5px 10px grey"}}>
                    <div className="card-body" style={{background:"#ffffff"}}>
                        <ul className="nav">
                            <li className="nav-item">
                                <Link className="nav-link active" style={{color:"#47505C"}}  aria-current="page" to={"/course"}>Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}} >Code Playground</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Discuss</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Blog</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" style={{color:"#47505C"}}> Get Pro</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="card-footer" style={{background:"#ffffff"}}>

                        <Link to={"/admin/login"}>
                            <button className="login">Log in</button>
                        </Link>
                        <Link to={"/admin/register"}>
                            <button className="register">Register</button>
                        </Link>
                    </div>
                </div> : ''}
            </nav> : <nav className="navbar mobile-navbar">
                <div className="container-fluid mobile">
                    <Link exact={true} to={"/"} className="navbar-brand tablet-brand" href="#">
                        <img src="img/logo-mobile.png" alt="logo-black"/>
                    </Link>
                    <div className="left">
                        <span>Menu</span>
                        <div className="menu-icon" onClick={handleClick}>
                            <i className={click ? "fas fa-times" : "fas fa-bars"}/>
                        </div>
                    </div>
                </div>
                {click ? <div className="card">
                    <div className="card-body">
                        <ul className="nav">
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={"/course"}>Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" >Code Playground</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Discuss</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Blog</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link"> Get Pro</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="card-footer">

                        <Link to={"/admin/login"}>
                            <button className="login">Log in</button>
                        </Link>
                        <Link to={"/admin/register"}>
                            <button className="register">Register</button>
                        </Link>
                    </div>
                </div> : ''}
            </nav>}


        </>);
}

export default Navbar;