import React from 'react';
import "./community.scss"
function Community(props) {
    return (
        <div className="community">
            <h1 className="title">Supportive community</h1>
            <div className="box">
                <video src="gif/gif.mp4"   autoPlay muted loop />
            </div>
        </div>
    );
}

export default Community;