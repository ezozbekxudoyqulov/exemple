import React from 'react';
import "./technical.scss"

function Technical(props) {
    return (
        <div className=" technical">
            <div className="wrapper">
                <h1 className="title">
                    The perfect platform to boost your technical skills </h1>
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <img src="img/student.svg" alt="student"/>
                            </div>
                            <div className="card-body">
                                <p className="titles">Students</p>
                                <div className="text-first">
                                    Prepping for the big test or
                                    want to ace your first interview? Use Sololearn's
                                    real-world practice to reinforce what
                                    you've learned and get you ready for that big
                                    moment.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <img src="img/professional.svg" alt="student"/>
                            </div>
                            <div className="card-body">
                                <p className="titles">Professionals</p>
                                <div className="text-first">
                                    You can learn something totally new to
                                    advance your career. Or maybe you just want
                                    to knock off the rust. Try Sololearn to get
                                    access to a variety of courses, from
                                    machine learning to web development.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Technical;