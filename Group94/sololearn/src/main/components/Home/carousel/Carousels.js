import React from 'react';
import Slider from "react-slick";
import "./carousels.scss"

function Carousels(props) {
    var settings = {
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        lazyLoad: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    return (
        <div className="Carousels">
        <div className="box">
            <Slider {...settings}>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>
                <div className="images">
                <div className="radius">
                    <img src="https://sololearnuploads.azureedge.net/uploads/courses/1073.png" alt=""/>
                </div>
                </div>


            </Slider>
        </div>
        </div>
    );
}

export default Carousels;