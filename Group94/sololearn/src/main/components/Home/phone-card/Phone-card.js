import React from 'react';
import "./phone-card.scss"
import {Link} from "react-router-dom";

function PhoneCard(props) {
    return (
        <div className="phone-card">
            <div className="wrapper">
                <div className="row">
                    <div className=" col-lg-8 desktop">
                        <div className="box">
                            <div className="box-body">
                                <h1 className="title">Learning on the go</h1>
                                <p className="text">
                                    <span>Learn on the web or on the go.</span><br/>
                                    Sololearn is always ready for you and you'll
                                    never lose your place.

                                </p>
                                <div className="play-store">
                                    <a href="https://itunes.apple.com/us/app/sololearn-learn-to-code/id1210079064?mt=8"
                                       target="_blank">
                                        <button className="btn app">
                                            <img src="img/btn-app.png" alt="app"/>
                                        </button>
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.sololearn"
                                       target="_blank">
                                        <button className="btn play">
                                            <img src="img/btn-play.png" alt="play"/>
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div className="box-body">
                                <img src="img/phone1.png" alt="phone"/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-4 left">
                        <div className="card">
                            <div className="card-header">
                                <img src="img/home-why-solo-5.svg" alt=""/>
                            </div>
                            <div className="card-body">
                                <h1 className="title">More than 20 courses</h1>
                                <p className="text">From Python, through data, to web dev. <span>We got everything you need.</span></p>
                                <Link to={"/course"}> <p className="link">Go to courses ></p></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}


export default PhoneCard;