import React from 'react';
import Navbar from "./Navbars/Navbar";
import Header from "./Header/Header";
import Cards from "./person-card/cards";
import PhoneCard from "./phone-card/Phone-card";
import Technical from "./technical-card/technical";
import Community from "./community/community";
import Carousels from "./carousel/Carousels";

function Home(props) {
    return (
        <>
            <Navbar/>
            <Header/>
            <Carousels/>
            <Cards/>
            <PhoneCard/>
            <Technical/>
            <Community/>
            <Cards/>

        </>
    );
}

export default Home;