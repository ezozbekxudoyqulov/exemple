import React from 'react';
import './cards.scss'
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
function Cards(props) {
    return (
        <div className="wrapper">
            <div className="row">
                <Fade left>
                <div className="col-md-6 col-lg-4">

                    <div className="card">
                        <div className="card-header">
                            <img src="img/home-why-solo-1.svg" alt=""/>
                        </div>
                        <div className="card-body">
                            <h1 className="title">
                                Tailored to you
                            </h1>
                            <p className="text">No matter your experience level, you'll be
                                writing <span>real, functional code within minutes </span>of
                                starting your first course. </p>
                        </div>
                    </div>
                </div>
                </Fade>
                <Zoom >
                <div className="col-md-6 col-lg-4">
                    <div className="card">
                        <div className="card-header">
                            <img src="img/home-why-solo-2.svg" alt=""/>
                        </div>
                        <div className="card-body">
                            <h1 className="title">
                                Bite-sized
                            </h1>
                            <p className="text">Go step-by-step through our unique courses.
                                Assess what you’ve learned with in-lesson quizzes, and gradually
                                <br/><span>advance your skills with practice.</span></p>
                        </div>
                    </div>

                </div>
                </Zoom>
                <Fade right>
                <div className="col-md-6 col-lg-4">
                    <div className="card">
                        <div className="card-header">
                            <img src="img/home-why-solo-3.svg" alt=""/>
                        </div>
                        <div className="card-body">
                            <h1 className="title">
                                Get proof
                            </h1>
                            <p className="text"><span>Earn a certificate</span>
                                to validate your newly acquired skills.
                                Post it on social for others to see.
                            </p>
                        </div>
                    </div>

                </div>
                </Fade>
            </div>
        </div>
    );
}

export default Cards;