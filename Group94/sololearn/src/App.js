import React from 'react';
import {Switch, Route} from "react-router-dom";
import Authentication from "./Admin/component/authentification/authentification";
import UserAdd from "./Admin/component/authentification/user-add/user-add";
import Main from "./Admin/component/layout/main";
import Home from "./main/components/Home/Home";
import Course from "./main/components/course/course";
import Chapter from "./main/components/Chapter/chapter";
function App(props) {

    return (
        <Switch>
            <Route exact={true} path={"/"} component={Home}/>
            <Route exact={true} path={"/course"} component={Course}/>
            <Route  path={"/course/chapter:id"} component={Chapter}/>
            <Route path={"/admin/main"} component={Main}/>
            <Route path={"/admin/register"} component={UserAdd}/>
            <Route path={"/admin/login"} component={Authentication}/>
        </Switch>
    );

}

export default App;